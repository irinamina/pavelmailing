package com.irinaminakova.pavelmailing

data class Message (val id: Int, val phone: Long, val message: String)
