package com.irinaminakova.pavelmailing

import com.google.gson.Gson
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

class Request {

    companion object {
        const val GET_MESSAGES = "http://sms.shirmanoff.ru/api/message"
        const val POST_MESSAGE = "http://sms.shirmanoff.ru/api/message/"
        const val STATUS_SUCCESS = "success"
        const val STATUS_FAIL = "fail"
    }

    fun getMessages(): List<Message> {
        val url = URL(GET_MESSAGES)
        val responseString = url.readText()
        val array =  Gson().fromJson(responseString, Array<Message>::class.java)
        return array.toList()
    }

    fun postMessage(id: Int, success: Boolean) {

        val mURL = URL("$POST_MESSAGE$id")

        with(mURL.openConnection() as HttpURLConnection) {
            requestMethod = "POST"

            val status = if (success) STATUS_SUCCESS else STATUS_FAIL
            val reqParam = URLEncoder.encode("status", "UTF-8") + "=" + URLEncoder.encode(status, "UTF-8")

            val wr = OutputStreamWriter(outputStream);
            wr.write(reqParam);
            wr.flush();

            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()

                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
                println("Response : $response")
            }
        }
    }
}