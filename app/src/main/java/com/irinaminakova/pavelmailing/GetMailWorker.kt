package com.irinaminakova.pavelmailing

import android.Manifest
import android.app.Activity
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.telephony.SmsManager
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.work.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit


class GetMailWorker(private val appContext: Context, workerParams: WorkerParameters):
    Worker(appContext, workerParams) {

    companion object {
        const val TAG = "GetMailWorker"

        const val SENT = "SMS_SENT"
        const val DELIVERED = "SMS_DELIVERED"

        fun createMailWorker() : WorkRequest {
            val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.UNMETERED) //wi-fi
                .build()

            // The minimum repeat interval that can be defined with PeriodicWorkRequestBuilder is 15 minutes
            return PeriodicWorkRequestBuilder<GetMailWorker>(
                PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, TimeUnit.MILLISECONDS)
                .setConstraints(constraints)
                .setBackoffCriteria(
                    BackoffPolicy.LINEAR, PeriodicWorkRequest.MIN_BACKOFF_MILLIS,
                    TimeUnit.MILLISECONDS)
                .addTag(TAG)
                .build()
        }

        fun WorkManager.isWorkScheduled(tag: String):Boolean {
            val infosFuture = this.getWorkInfosByTag(tag)
            return try {
                val infos = infosFuture.get()
                infos.any {
                    it.state == WorkInfo.State.RUNNING || it.state == WorkInfo.State.ENQUEUED
                }
            } catch (e : Exception) {
                e.printStackTrace()
                false
            }
        }
    }

    override fun doWork(): Result {
        val list = Request().getMessages()
        val smsManager = SmsManager.getDefault()
        list.forEach{
            Log.d(TAG, it.toString())

            if (ContextCompat.checkSelfPermission(appContext, Manifest.permission.SEND_SMS) !=
                    PackageManager.PERMISSION_GRANTED) {
                Request().postMessage(it.id, false)
            } else {
                sendSms(it, smsManager)
            }
        }

        //todo: If you change it to "success", you can remove "setBackoffCriteria"
        // from PeriodicWorkRequestBuilder and it would run every 15 minutes
        return Result.retry()
    }

    private fun sendSms(msg: Message, smsManager: SmsManager) {
        val sentPI = PendingIntent.getBroadcast(appContext, 0, Intent(SENT), 0)
        val deliveredPI = PendingIntent.getBroadcast(appContext, 0, Intent(DELIVERED), 0)
        appContext.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(arg0: Context?, arg1: Intent?) {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        //do nothing, wait for DELIVERED broadcast
                    }
                    else -> {
                        //SmsManager.RESULT_ERROR_GENERIC_FAILURE, SmsManager.RESULT_ERROR_NO_SERVICE
                        //SmsManager.RESULT_ERROR_NULL_PDU or SmsManager.RESULT_ERROR_RADIO_OFF
                        Request().postMessage(msg.id, false)
                    }
                }
            }
        }, IntentFilter(SENT))
        appContext.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(arg0: Context?, arg1: Intent?) {
                GlobalScope.launch(Dispatchers.IO) {
                    Request().postMessage(msg.id, resultCode == Activity.RESULT_OK)
                }
            }
        }, IntentFilter(DELIVERED))

        if (BuildConfig.FLAVOR == "demo") {
            smsManager.sendTextMessage("0630", null, "10", sentPI, deliveredPI)
        } else {
            smsManager.sendTextMessage(msg.phone.toString(), null, msg.message, sentPI, deliveredPI)
        }



    }
}