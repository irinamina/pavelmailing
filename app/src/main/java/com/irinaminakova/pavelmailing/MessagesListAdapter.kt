package com.irinaminakova.pavelmailing

import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MessagesListAdapter(private val items: List<Message>) : RecyclerView.Adapter<MessagesListAdapter.ViewHolder>() {

    class ViewHolder (val textView: TextView) : RecyclerView.ViewHolder(textView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(TextView(parent.context))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val msg = items[position]
        holder.textView.text = "${msg.phone}: ${msg.message}"
    }
}