package com.irinaminakova.pavelmailing

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.widget.Button
import android.widget.ToggleButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.*
import com.irinaminakova.pavelmailing.GetMailWorker.Companion.isWorkScheduled
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var workManager: WorkManager
    private lateinit var toggle: ToggleButton
    private lateinit var buttonSend: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        workManager = WorkManager.getInstance(this)
        toggle = findViewById(R.id.toggle)
        buttonSend = findViewById(R.id.buttonSend)
        val recyclerView = findViewById<RecyclerView>(R.id.list)
        recyclerView.layoutManager = LinearLayoutManager(this)

        GlobalScope.launch(Dispatchers.IO) {
            val list = Request().getMessages()
            GlobalScope.launch(Dispatchers.Main) {
                recyclerView.adapter = MessagesListAdapter(list)
            }
        }

        toggle.isChecked = workManager.isWorkScheduled(GetMailWorker.TAG)
        toggle.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                //todo: check for send sms permission
                if (ContextCompat.checkSelfPermission(applicationContext,
                        Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                    val getMailWorker = GetMailWorker.createMailWorker()
                    workManager.enqueue(getMailWorker)
                } else {
                    ActivityCompat.requestPermissions(this@MainActivity,
                        arrayOf(Manifest.permission.SEND_SMS), 1);
                }
            }
            else {
                workManager.cancelAllWorkByTag(GetMailWorker.TAG)
            }
        }

        buttonSend.setOnClickListener {
            val messageToSend = "10"
            val number = "0630"

            SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null, null)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults:IntArray) {
        if (requestCode != 1) {
            return
        }
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            val getMailWorker = GetMailWorker.createMailWorker()
            workManager.enqueue(getMailWorker)
        } else {
            toggle.isChecked = false
            // The service is unavailable because it requires a SEND_SMS permission
        }
    }

}